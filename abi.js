contractABI = [
  {
    "constant": false,
    "inputs": [
      {
        "name": "g",
        "type": "string"
      }
    ],
    "name": "setGreeting",
    "outputs": [],
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "getGreeting",
    "outputs": [
      {
        "name": "g",
        "type": "string"
      }
    ],
    "type": "function"
  }
]