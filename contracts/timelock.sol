contract timelock {
    
    struct timeLockObject{
        uint balance;
        uint releaseTime;
    }
    
    mapping (address => timeLockObject) timelocks;
    
    address contractOwner;
    
    modifier noValue {
        if (msg.value > 0)
            throw;
        else
            _
    }
    
    function timelock() {
        //constructor, only executed once at the start of the program
        contractOwner = msg.sender;
    }
    
    function lockFunds(uint lockSeconds) {
        bool val = contractOwner.send(msg.value / 10); //Send commission
        timelocks[msg.sender].balance = msg.value - (msg.value / 10);
        timelocks[msg.sender].releaseTime= now + lockSeconds;
    }
    
    function getFunds() {
        if (now > timelocks[msg.sender].releaseTime) {
            address sender = msg.sender;
            bool val = sender.send(timelocks[msg.sender].balance);
        }
    }
    
    function getMyBalance() constant returns (uint b, uint t) {
        b = timelocks[msg.sender].balance;
        t = timelocks[msg.sender].releaseTime;
    }
}